package com.example.fragmentsample;


import android.R.integer;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class myFragment extends Fragment {
	OnClickListener mCallback;

    // The container Activity must implement this interface so the frag can deliver messages
    public interface OnClickListener {
        /** Called by HeadlinesFragment when a list item is selected */
        public void onClick(int index);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	Log.d("Ted","Create");
        super.onCreate(savedInstanceState);
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnClickListener");
        }
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_layout, container, false);
        Button btn = (Button)v.findViewById(R.id.ClickBtn);
        Button btn2 = (Button)v.findViewById(R.id.ClickBtn2);
        final TextView textview = (TextView)v.findViewById(R.id.Hello);
        btn2.setOnClickListener(new Button.OnClickListener()
        {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				textview.setText("click");								
			}        	
        });
        btn.setOnClickListener(new Button.OnClickListener()
        {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub				
				mCallback.onClick(1);
				
			}        	
        });
        return v;
    }

}
