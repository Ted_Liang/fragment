package com.example.fragmentsample;



import android.R.integer;
import android.app.Activity;
import android.os.Bundle;
import android.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity 
	implements myFragment.OnClickListener{
	
	myFragment myFragment = new myFragment();
	NxtFragment nxtFragment = new NxtFragment();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
        FragmentTransaction transaction = this.getFragmentManager().beginTransaction();

        transaction.add(R.id.rela, myFragment, "first");
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commit();
        
        FragmentTransaction transaction2 = this.getFragmentManager().beginTransaction();
        transaction2.hide(myFragment);												
        transaction2.add(R.id.rela, nxtFragment);
        transaction2.addToBackStack(null);
        transaction2.commit();
	}
    
	public void onClick(int index)
	{
		Log.d("Ted","what");
		if(index==1)
		{
			
//	        FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
//	        transaction.hide(myFragment);												
//	        transaction.add(R.id.rela, nxtFragment);
//	        transaction.addToBackStack(null);
//	        transaction.commit();
			Log.d("Ted","1");
			FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
			transaction.hide(myFragment);	
			transaction.show(nxtFragment);	
			transaction.commit();
		}
		else 
		{
			
//	        FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
//	        transaction.hide(nxtFragment);																
//	        transaction.add(R.id.rela, myFragment);
//	        transaction.addToBackStack(null);
//	        transaction.commit();
			Log.d("Ted","2");
			FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
			transaction.hide(nxtFragment);	
			transaction.show(myFragment);	
			transaction.commit();
				
			
		}
	}
}
